#include "AnalogInputPlugin.h"

AnalogInputPlugin::AnalogInputPlugin(GpioConfig cfg){
    config = cfg;
}
void AnalogInputPlugin::activate(Scheduler *userScheduler)
{
    // add update task
    inputTask.set(TASK_MILLISECOND * config.updateInterval, TASK_FOREVER, std::bind(&AnalogInputPlugin::checkInput, this));
    userScheduler->addTask(inputTask);
    inputTask.enable();

    // add dummy subscription
    subscribe(config.topic, [](String msg){});

    PRINT_MSG(Serial, "PLUGIN", "AnalogInputPlugin activated");
}

void AnalogInputPlugin::checkInput()
{
    int newVal = analogRead(config.pin);
    if ((newVal >= currentVal + config.threshold || newVal <= currentVal - config.threshold))
    {
        publish(config.topic, String(newVal));
        currentVal = newVal;
    }
}
