#include "AudioInputPlugin.h"

AudioInputPlugin::AudioInputPlugin(GpioConfig cfg){
    config = cfg;
} 
void AudioInputPlugin::activate(Scheduler *userScheduler)
{
    // add update task
    inputTask.set(TASK_MILLISECOND * config.updateInterval, TASK_FOREVER, std::bind(&AudioInputPlugin::checkInput, this));
    userScheduler->addTask(inputTask);
    inputTask.enable();

    // add dummy subscription
    subscribe(config.topic, [](String msg){});

    PRINT_MSG(Serial, "PLUGIN", "AudioInputPlugin activated");
}

void AudioInputPlugin::checkInput()
{
    startMillis= millis();
            peakToPeak = 0;
            signalMax = 0;
            signalMin = 1024;
            // collect data 
            while (millis() - startMillis < sampleWindow) {
                sample = analogRead(A0);
                if (sample < 1024) {
                    signalMax = sample > signalMax ? sample : signalMax; 
                    signalMin = sample < signalMin ? sample : signalMin;
                    
                }
            }
            peakToPeak = signalMax - signalMin;
            
            int val = mapValueToRange(peakToPeak, 0, 1024, 32, 255);
            if(val != lastVal){
                publish(config.topic, String(val));
                lastVal = val;
            }
}
