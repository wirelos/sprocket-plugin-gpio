#ifndef __AUDIO_INPUT__PLUGIN_H__
#define __AUDIO_INPUT__PLUGIN_H__

#define _TASK_SLEEP_ON_IDLE_RUN
#define _TASK_STD_FUNCTION

#include <functional>
#include <vector>
#include <FS.h>
#include <TaskSchedulerDeclarations.h>
#include <Plugin.h>
#include <utils/print.h>
#include <utils/misc.h>
#include <GpioConfig.h>

using namespace std;
using namespace std::placeholders;

class AudioInputPlugin : public Plugin
{
  private:
    const int sampleWindow = 50; // Sample window width in mS (50 mS = 20Hz)
    unsigned int sample;
    unsigned long startMillis = millis();
    unsigned int peakToPeak = 0;
    unsigned int signalMax = 0;
    unsigned int signalMin = 1024;
    unsigned int lastVal = 0;

  public:
    Task inputTask;
    int currentVal = 0;
    GpioConfig config;
    AudioInputPlugin(GpioConfig cfg);
    void activate(Scheduler *userScheduler);
    void checkInput();
};

#endif
