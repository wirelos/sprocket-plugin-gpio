#ifndef __DIGITAL_INPUT__PLUGIN_H__
#define __DIGITAL_INPUT__PLUGIN_H__

#define _TASK_SLEEP_ON_IDLE_RUN
#define _TASK_STD_FUNCTION 

#include <functional>
#include <vector>
#include <FS.h>
#include <TaskSchedulerDeclarations.h>
#include <Plugin.h>
#include <utils/print.h>
#include <utils/misc.h>
#include <GpioConfig.h>

using namespace std;
using namespace std::placeholders;

class DigitalInputPlugin : public Plugin {
    public:
        Task inputTask;
        int currentVal = 0;
        GpioConfig config;
        DigitalInputPlugin(GpioConfig cfg);
        void activate(Scheduler* userScheduler);
        virtual void checkInput();
};

#endif
