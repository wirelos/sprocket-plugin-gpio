#include "DigitalInputPlugin.h"

DigitalInputPlugin::DigitalInputPlugin(GpioConfig cfg)
{
    config = cfg;
    if (config.pinMode)
    {
        pinMode(config.pin, config.pinMode);
    }
}
void DigitalInputPlugin::activate(Scheduler *userScheduler)
{
    // add update task
    inputTask.set(TASK_MILLISECOND * config.updateInterval, TASK_FOREVER, std::bind(&DigitalInputPlugin::checkInput, this));
    userScheduler->addTask(inputTask);
    inputTask.enable();

    // add dummy subscription
    subscribe(config.topic, [](String msg){});

    PRINT_MSG(Serial, "PLUGIN", "DigitalInputPlugin activated");
}

void DigitalInputPlugin::checkInput()
{
    int newVal = digitalRead(config.pin);
    if (newVal != currentVal)
    {
        publish(config.topic, String(newVal));
        currentVal = newVal;
    }
}
