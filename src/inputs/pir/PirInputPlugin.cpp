#include "PirInputPlugin.h"

void PirInputPlugin::checkInput()
{
    
    if(startupDelay > millis()){
        return;
    }
    val = digitalRead(config.pin); // read input value
    if (val == HIGH)
    { // check if the input is HIGH
        if (pirState == LOW)
        {
            // we have just turned on
            publish(config.topic, "1");
            // We only want to print on the output change, not state
            pirState = HIGH;
        }
    }
    else
    {
        if (pirState == HIGH)
        {
            // we have just turned of
            publish(config.topic, "0");
            // We only want to print on the output change, not state
            pirState = LOW;
        }
    }
}
