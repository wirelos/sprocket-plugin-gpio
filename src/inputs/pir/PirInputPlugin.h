#ifndef __PIR_INPUT__PLUGIN_H__
#define __PIR_INPUT__PLUGIN_H__

#define _TASK_SLEEP_ON_IDLE_RUN
#define _TASK_STD_FUNCTION 

#include <functional>
#include <vector>
#include <FS.h>
#include <TaskSchedulerDeclarations.h>
#include <Plugin.h>
#include <utils/print.h>
#include <utils/misc.h>
#include <inputs/digital/DigitalInputPlugin.h>
#include <GpioConfig.h>

using namespace std;
using namespace std::placeholders;

class PirInputPlugin : public DigitalInputPlugin {
    public:
        PirInputPlugin(GpioConfig cfg) : DigitalInputPlugin(cfg){}
        int pirState = LOW;             // we start, assuming no motion detected
        int val = 0;  
        int startupDelay = 20000; // TODO move to cfg
        void checkInput();
};

#endif
