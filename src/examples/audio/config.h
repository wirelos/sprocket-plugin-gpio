#ifndef __DEVICE_CONFIG__
#define __DEVICE_CONFIG__

// Scheduler
#define _TASK_SLEEP_ON_IDLE_RUN
#define _TASK_STD_FUNCTION
#define _TASK_PRIORITY

// Chip
#define SPROCKET_TYPE       "SPROCKET"
#define SERIAL_BAUD_RATE    115200
#define STARTUP_DELAY       1000

// audio
#define AUDIO_THRESHOLD 0
#define AUDIO_POLL_INTERVAL 200
#define AUDIO_PIN A0
#define AUDIO_TOPIC "audio/peak"

#endif