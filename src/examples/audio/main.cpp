#include "config.h"
#include "Sprocket.h"
#include "inputs/audio/AudioInputPlugin.h"

Sprocket *sprocket;

void setup()
{
    sprocket = new Sprocket({STARTUP_DELAY, SERIAL_BAUD_RATE});
    sprocket->addPlugin(
        new AudioInputPlugin({AUDIO_PIN, AUDIO_THRESHOLD, AUDIO_POLL_INTERVAL, AUDIO_TOPIC}));
    sprocket->subscribe(AUDIO_TOPIC, bind([](String val) {
        PRINT_MSG(Serial, "AUDIO", val.c_str());
    }, _1));
    sprocket->activate();
}

void loop()
{
    sprocket->loop();
    yield();
}