#include "config.h"
#include "Sprocket.h"
#include "inputs/pir/PirInputPlugin.h"

Sprocket *sprocket;

void setup()
{
    sprocket = new Sprocket({STARTUP_DELAY, SERIAL_BAUD_RATE});
    sprocket->addPlugin(
        new PirInputPlugin({PIR_PIN, PIR_THRESHOLD, PIR_POLL_INTERVAL, PIR_TOPIC}));
    sprocket->subscribe(PIR_TOPIC, bind([](String label, String val){
        PRINT_MSG(Serial, label.c_str(), val.c_str());
    }, PIR_TOPIC, _1));
    sprocket->activate();
}

void loop()
{
    sprocket->loop();
    yield();
}