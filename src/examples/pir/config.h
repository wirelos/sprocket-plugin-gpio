#ifndef __DEVICE_CONFIG__
#define __DEVICE_CONFIG__

// Scheduler
#define _TASK_SLEEP_ON_IDLE_RUN
#define _TASK_STD_FUNCTION
#define _TASK_PRIORITY

// Chip
#define SPROCKET_TYPE       "SPROCKET"
#define SERIAL_BAUD_RATE    115200
#define STARTUP_DELAY       1000

// PIR
#define PIR_THRESHOLD 16
#define PIR_POLL_INTERVAL 50
#define PIR_TOPIC "pir"
#define PIR_PIN D3

#endif