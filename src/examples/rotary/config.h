#ifndef __DEVICE_CONFIG__
#define __DEVICE_CONFIG__

// Scheduler
#define _TASK_SLEEP_ON_IDLE_RUN
#define _TASK_STD_FUNCTION
#define _TASK_PRIORITY

// Chip
#define SPROCKET_TYPE       "SPROCKET"
#define SERIAL_BAUD_RATE    115200
#define STARTUP_DELAY       1000

/*
connecting Rotary encoder
CLK (A pin) - to any microcontroler intput pin with interrupt
DT (B pin) - to any microcontroler intput pin with interrupt
SW (button pin) - to any microcontroler intput pin
VCC - to microcontroler VCC (then set ROTARY_ENCODER_VCC_PIN -1)
GND - to microcontroler GND
*/
#define ROTARY_ENCODER_A_PIN 35
#define ROTARY_ENCODER_B_PIN 34
#define ROTARY_ENCODER_BUTTON_PIN 21
#define ROTARY_ENCODER_VCC_PIN -1 /*put -1 of Rotary encoder Vcc is connected directly to 3,3V; else you can use declared output pin for powering rotary encoder */

#endif