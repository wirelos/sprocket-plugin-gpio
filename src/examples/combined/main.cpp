#include "config.h"
#include "Sprocket.h"
#include "inputs/rotary/RotaryPlugin.h"
#include "inputs/analog/AnalogInputPlugin.h"
#include "inputs/digital/DigitalInputPlugin.h"

Sprocket *sprocket;
RotaryPlugin *r1;

void addRotary(Sprocket *s, Plugin *r, const char *topic)
{
    String btnTopic = String(topic) + String("/button");
    s->subscribe(topic, bind([](String label, String val) { PRINT_MSG(Serial, label.c_str(), val.c_str()); }, topic, _1));
    s->subscribe(btnTopic, bind([](String label, String val) { PRINT_MSG(Serial, label.c_str(), val.c_str()); }, btnTopic, _1));
    s->addPlugin(r);
}

void addAnalogInput(Sprocket* s, int pin, const char* topic)
{
    s->subscribe(topic, bind([](String label, String val){
        PRINT_MSG(Serial, label.c_str(), val.c_str());
    }, topic, _1));
    s->addPlugin(new AnalogInputPlugin({pin, POT_THRESHOLD, POT_POLL_INTERVAL, topic}));
}

void addDigitalInput(Sprocket* s, int pin, const char* topic)
{
    s->subscribe(topic, bind([](String label, String val){
        PRINT_MSG(Serial, label.c_str(), val.c_str());
    }, topic, _1));
    s->addPlugin(new DigitalInputPlugin({pin, POT_THRESHOLD, POT_POLL_INTERVAL, topic, INPUT}));
}


void setup()
{
    sprocket = new Sprocket({STARTUP_DELAY, SERIAL_BAUD_RATE});
    r1 = new RotaryPlugin({35, 34, 21, -1, 50, 0, 255, true, "rotary1"});
    r1->rotaryEncoder->setup([] { r1->rotaryEncoder->readEncoder_ISR(); });
    addRotary(sprocket, r1, "rotary1");
    addAnalogInput(sprocket, 36, "pot1");
    addAnalogInput(sprocket, 37, "pot2");
    addAnalogInput(sprocket, 38, "pot3");
    addAnalogInput(sprocket, 39, "pot4");
    addDigitalInput(sprocket, 21, "btn");
    sprocket->activate();
}

void loop()
{
    sprocket->loop();
    yield();
}