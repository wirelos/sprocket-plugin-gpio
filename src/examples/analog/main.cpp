#include "config.h"
#include "Sprocket.h"
#include "inputs/analog/AnalogInputPlugin.h"

Sprocket *sprocket;

void registerPot(Sprocket* s, int pin, const char* topic)
{
    s->addPlugin(
        new AnalogInputPlugin({pin, POT_THRESHOLD, POT_POLL_INTERVAL, topic}));
    s->subscribe(topic, bind([](String label, String val){
        PRINT_MSG(Serial, label.c_str(), val.c_str());
    }, topic, _1));
}

void setup()
{
    sprocket = new Sprocket({STARTUP_DELAY, SERIAL_BAUD_RATE});
    registerPot(sprocket, 36, "pot1");
    registerPot(sprocket, 37, "pot2");
    registerPot(sprocket, 38, "pot3");
    registerPot(sprocket, 39, "pot4");
    sprocket->activate();
}

void loop()
{
    sprocket->loop();
    yield();
}