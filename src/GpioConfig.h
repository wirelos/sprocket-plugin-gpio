#ifndef __GPIO_CONFIG_
#define __GPIO_CONFIG_

struct GpioConfig
{
    int pin;
    int threshold;
    int updateInterval;
    const char *topic;
    int pinMode;
};

#endif